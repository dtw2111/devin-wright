import "./ResumeDownload.css";

const ResumeDownload = () => {
  const resume = process.env.PUBLIC_URL + "/Devin Wright - Resume.pdf";

  return (
    <div>
      <a href={resume} target="_blank" rel="noreferrer">
        <button className="resume">Resume PDF</button>
      </a>
    </div>
  );
};

export default ResumeDownload;
