import React from "react";
// import PropTypes from "prop-types";
import { Nav, Navbar as BSNavbar, Container } from "react-bootstrap";
import styles from "./Navbar.module.css";
import { useNavigate } from "react-router-dom";
import ResumeDownload from "./ResumeDownload";

const Navbar = (props) => {
  const navigate = useNavigate();

  const selectNavLink = (page) => {
    return () => {
      props.collapseNav();
      navigate(page);
    };
  };

  const email = {
    subject: "We'd love to work with you! 🎉",
    body: "Hi Devin,%0D%0A%0D%0AOur company would love to get in touch with you about potentially working with us. Please reach out to ____________________ for more details about this opportunity.%0D%0A%0D%0AWe look forward to hearing from you soon!%0D%0A%0D%0ABest of luck to you on your job search,%0D%0A%0D%0A____________________",
  };
  for (const [k, v] in Object.entries(email)) {
    email[k] = encodeURIComponent(v);
  }

  return (
    <BSNavbar
      expand="md"
      expanded={props.expanded}
      onSelect={props.collapseNav}
      collapseOnSelect="true"
      className={`bg-body-tertiary ${styles.Navbar}`}
      // style={{
      //   borderBottom: "1px solid",
      // }}
    >
      <Container
        fluid
        style={{ height: "100%", paddingLeft: "0px", paddingRight: "0px" }}
      >
        <BSNavbar.Brand
          id="brand"
          className={`display fw-bold ${styles["nav-item"]} ${styles.brand}`}
          onClick={() => {
            navigate("/");
            props.collapseNav();
          }}
        >
          <button className={styles.navtext}>DW</button>
        </BSNavbar.Brand>

        <Nav className={`ms-auto mx-3 m-2 ${styles.showmd}`}>
          <ResumeDownload />
        </Nav>

        <button
          className={`navbar-toggler me-2 ${styles.navbarToggler}`}
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="navbarNav"
          aria-controls="basic-navbar-nav"
          aria-expanded="false"
          aria-label="Toggle navigation"
          onClick={props.toggleNav}
        >
          <div
            className={`${styles.hamburger}${
              props.expanded ? " " + styles.open : ""
            }`}
          >
            <span></span>
            <span></span>
            <span></span>
          </div>
        </button>

        <BSNavbar.Collapse id="basic-navbar-nav">
          <Nav onSelect={(selectedKey) => navigate(selectedKey)}>
            <li
              id="about"
              className={"nav-item " + styles["nav-item"] + " " + styles.about}
              onClick={selectNavLink("/about")}
            >
              <button className={styles.navtext}>About</button>
            </li>
            <li
              id="resume"
              className={"nav-item " + styles["nav-item"] + " " + styles.resume}
              onClick={selectNavLink("/resume")}
            >
              <button className={styles.navtext}>Resume</button>
            </li>

            <a
              href={`mailto:devin.wright.software@gmail.com?subject=${email.subject}&body=${email.body}`}
              target="_blank"
              rel="noreferrer"
              style={{ all: "unset" }}
            >
              <li
                id="hireme"
                className={
                  "nav-item " + styles["nav-item"] + " " + styles.hireme
                }
              >
                <button className={styles.navtext}>Hire Me</button>
              </li>
            </a>
            {/* <li
              id="notfound"
              className={
                "nav-item " + styles["nav-item"] + " " + styles.notfound
              }
              onClick={selectNavLink("/404")}
            >
              404
            </li> */}
          </Nav>
        </BSNavbar.Collapse>

        <div className={`ms-auto mx-3 m-2 ${styles.hidemd}`}>
          <ResumeDownload />
        </div>
      </Container>
    </BSNavbar>
  );
};

Navbar.propTypes = {};

Navbar.defaultProps = {};

export default Navbar;
