import React, { useEffect } from "react";
// import PropTypes from "prop-types";
import styles from "./NotFound.module.css";
// import Wordle from "../Wordle/Wordle";

const NotFound = (props) => {
  useEffect(() => {
    props.newColor("--not-found-color");
  });

  return (
    <div className={styles.NotFound}>
      <h1 className="display-1">Whoops.</h1>
      <h3>This path doesn't exist (yet).</h3>
      {/* <p>Play a game before you go?</p>
    <Wordle /> */}
    </div>
  );
};

NotFound.propTypes = {};

NotFound.defaultProps = {};

export default NotFound;
