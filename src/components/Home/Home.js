import React, { useState, useEffect } from "react";
// import PropTypes from "prop-types";
import "./Home.css";

// list of things I love
// don't make too long
// no period at the end
const words = [
  // tech
  "AWS",
  "LeetCode problems",
  "JavaScript",
  "Python",
  "React",
  "Angular",
  "CSS",
  "SQL",
  "Django",
  "HTML",
  "Mongo",
  "Bootstrap",
  "clever design",
  "coding",
  "RESTful APIs",
  "ServiceNow",
  "Git",
  "Agile development",
  "algorithms",

  // general
  "teaching",
  "to learn",
  "building websites",
  "collaborating",
  "staying organized",
  "happy users",
  "clever design",
  "optimization",

  // fun
  "coffee",
  "craft beers",
  "jazz music",
  "crossword puzzles",
  "reading sci-fi",
  "epic fantasy",
  "board games",
  "Settlers of Catan",
  "Magic: The Gathering",
  "pickleball",
];
// randomize words list
const shuffle = (arr) => {
  for (let i = arr.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
  return arr;
};

let wordData = shuffle(
  words.map((w, wi) => {
    return {
      wi: wi,
      w: w + ".",
      chars: (w + ".").split("").map((c, ci) => {
        return {
          ci: ci,
          c: c,
          className: `letter ${wi === 0 ? "in" : "out"}`,
          style: { opacity: 1 },
        };
      }),
    };
  })
);

const Home = (props) => {
  useEffect(() => {
    props.newColor("--home-color");
  });

  const startIdx = Math.floor(Math.random() * wordData.length);
  const [wordIdx, setWordIdx] = useState(startIdx);
  const [letterIdx, setLetterIdx] = useState(0);
  const [wordArray, setWordArray] = useState(wordData);

  useEffect(() => {
    const currWordLength = wordArray[wordIdx].w.length;
    const nextWordIdx = (wordIdx + 1) % wordArray.length;
    const nextWordLength = wordArray[nextWordIdx].w.length;
    const maxWordLength = Math.max(...wordArray.map((w) => w.w.length));

    const animate = (wi, ci, state) => {
      setWordArray((prevWordArray) => {
        prevWordArray[wi].chars[ci].className = `letter ${state}`;
        return prevWordArray;
      });
    };

    const [animateOut, animateBehind, animateIn] = [
      (wi, ci) => animate(wi, ci, "out"),
      (wi, ci) => animate(wi, ci, "behind"),
      (wi, ci) => animate(wi, ci, "in"),
    ];

    if (letterIdx === 0) {
      for (let i = 0; i < nextWordLength; i++) {
        animateBehind(nextWordIdx, i);
      }
    }

    const letterInterval = setInterval(() => {
      // clear lingering words
      for (let i = 0; i < wordArray.length; i++) {
        if (
          i !== nextWordIdx &&
          letterIdx < wordArray[i].w.length &&
          wordArray[i].chars[letterIdx].className !== "letter out"
        ) {
          animateOut(i, letterIdx);
        }
      }
      if (letterIdx < currWordLength) {
        animateOut(wordIdx, letterIdx);
      }
      if (letterIdx - 4 >= 0 && letterIdx - 4 < nextWordLength) {
        animateIn(nextWordIdx, letterIdx - 4);
      }
      if (letterIdx - 4 < maxWordLength) {
        setLetterIdx(letterIdx + 1);
      }
    }, 80);

    // update wordIdx
    const wordIdxTimer = setTimeout(() => {
      setWordIdx(nextWordIdx);
      setLetterIdx(0);
    }, 4000);

    return () => {
      clearInterval(letterInterval);
      clearTimeout(wordIdxTimer);
    };
  }, [wordIdx, letterIdx]);

  return (
    <div className="Home">
      <div style={{ maxWidth: "1000px", margin: "100px auto" }}>
        <div className="align-items-center g-5 px-4">
          <div className="mx-4">
            <h1 className="display-2 fw-bold lh-1 mb-3">Hi! I'm Devin.</h1>
            <div className="text">
              <p>
                I am a New York-based software developer who loves&nbsp;
                {wordArray
                  .filter(
                    (_w, i) =>
                      i === wordIdx ||
                      i === (wordIdx === wordArray.length - 1 ? 0 : wordIdx + 1)
                  )
                  .map((w) => (
                    <span className="word" key={w.wi}>
                      {w.chars.map((c) => {
                        if (c.c === " ") {
                          return (
                            <span
                              className={`${c.className}`}
                              style={c.style}
                              key={c.ci}
                            >
                              &nbsp;
                            </span>
                          );
                        }
                        return (
                          <span
                            className={`${c.className}`}
                            style={c.style}
                            key={c.ci}
                          >
                            {c.c}
                          </span>
                        );
                      })}
                    </span>
                  ))}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Home.propTypes = {};

Home.defaultProps = {};

export default Home;
