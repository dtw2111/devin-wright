import React, { useState, useEffect } from "react";
// import PropTypes from "prop-types";
import styles from "./Wordle.module.css";

const Wordle = () => {
  const [answer, setAnswer] = useState("");
  const [answerData, setAnswerData] = useState({});
  const [guesses, setGuesses] = useState([]);
  const [guessesRemaining, setGuessesRemaining] = useState(6);
  const [currGuess, setCurrGuess] = useState("");
  const [errorCheck, setErrorCheck] = useState(false);

  const addGuess = (word) => {
    setGuesses((prevGuesses) => {
      prevGuesses.push(word);
      setGuessesRemaining((prevGuessesRemaining) => prevGuessesRemaining - 1);
      return prevGuesses;
    });
  };

  useEffect(() => {
    if (errorCheck || !answer) {
      // fetch random word
      const randomWordUrl =
        "https://random-word-api.herokuapp.com/word?length=5";
      fetch(randomWordUrl)
        .then((res) => res.json())
        .then((data) => {
          const word = data[0];
          const wordDataUrl =
            "https://api.dictionaryapi.dev/api/v2/entries/en/" + word;

          // get definitions for word
          fetch(wordDataUrl)
            .then((res) => res.json())
            .then((data) => {
              if (data.title === "No Definitions Found") {
                console.error(
                  "Error: no definitions found for word. Fetching a new word..."
                );
                setErrorCheck(true);
              } else {
                setAnswer(word);
                setAnswerData(data);
                setErrorCheck(false);
              }
            })
            .catch((e) => {
              console.error(e);
              setErrorCheck(true);
            });
        })
        .catch((e) => {
          console.error(e);
          setErrorCheck(true);
        });
    }
  }, [errorCheck]);

  return (
    <div className={styles.Wordle}>
      <h2>Wordle</h2>
      <div className="gameBoard">
        <div className="guess pastGuesses">
          {guesses.map((guess, gidx) => {
            return guess.map((c, cidx) => (
              <div
                id={`guess${gidx + 1} letter${cidx + 1}`}
                className={`letter${
                  answer[cidx] === c
                    ? " correct"
                    : answer.includes(c)
                    ? " inWord"
                    : " absent"
                }
                ${
                  /*i === guesses[guesses.length - 1] ? " justGuessed" : ""*/ ""
                }`}
              >
                {c}
              </div>
            ));
          })}
        </div>
        <div className="guess newGuess"></div>

        <div className="guess remainingGuesses"></div>
      </div>
      <div className="keyBoard"></div>
    </div>
  );
};

Wordle.propTypes = {};

Wordle.defaultProps = {};

export default Wordle;
