import React, { useEffect, useState } from "react";
import "./Resume.css";

const getWindow = () => {
  const { innerWidth: width, innerHeight: height } = window;
  return { width, height };
};

const ResumeHTML = (props) => {
  useEffect(() => {
    props.newColor("--resume-color");
  });

  const [windowDimensions, setWindowDimensions] = useState(getWindow());
  useEffect(() => {
    const handleResize = () => {
      setWindowDimensions(getWindow());
    };
    window.addEventListener("resize", handleResize);
  }, []);

  return (
    <div className="resume-container">
      <section id="Title" className="px-5 my-5">
        <div className="container">
          <div className="row" style={{ alignItems: "center" }}>
            <div className="col-12 col-md-6 my-auto">
              {/* <h1 className="text-center pb-3">About Me</h1> */}
              <h1
                className={`display-4 ${
                  windowDimensions.width > 767
                    ? " text-end me-3"
                    : "text-center"
                }`}
              >
                Devin Wright
              </h1>
            </div>
            <div
              className={`col-12 col-md-6 text-center${
                windowDimensions.width > 767 ? " border-start" : ""
              }`}
            >
              <h2
                className={`fw-light ${
                  windowDimensions.width > 767
                    ? " text-start ms-3"
                    : "text-center"
                }`}
              >
                Full-Stack Software Engineer
              </h2>
            </div>
          </div>
        </div>
      </section>

      <section id="Experience">
        <div className="container">
          <div className="row">
            <h1 className="col-12 text-center fw-semibold py-3">Experience</h1>
          </div>
          <div className="row">
            <div className="col-12 col-md-6 px-5 py-4">
              <div style={{ width: "auto", textAlign: "center" }}>
                <h3 className="ps-2 py-2 fw-semibold d-inline-block">
                  Professional Experience
                </h3>
              </div>
              <div id="Smoothstack">
                <h5 className="mb-0">
                  <span className="fw-bold">
                    <a
                      href="https://smoothstack.com/"
                      style={{ all: "unset", cursor: "pointer" }}
                      target="_blank"
                      rel="noreferrer"
                    >
                      Smoothstack, Inc.
                    </a>
                  </span>{" "}
                  |{" "}
                  <span className="fw-normal fst-italic">
                    ServiceNow Associate
                  </span>
                </h5>
                <p className="mb-1 fw-light text-uppercase">
                  July 2023 - January 2024
                </p>
                <p>
                  Acquired ServiceNow development experience in an agile
                  environment by participating in and leading daily standup
                  meetings, using Confluence and Jira ticketing software to meet
                  biweekly sprint goals while developing Youneverse capstone
                  project.
                </p>
                <p>
                  Trained for Application Developer certification in ServiceNow
                  environment, gaining proficiency in a variety of ServiceNow
                  technologies like App Engine Studio, Flow Designer,
                  Integration Hub, and Service Portal.
                </p>
              </div>
              <div id="PMGillen">
                <h5 className="mb-0">
                  <span className="fw-bold">
                    <a
                      href="https://pmgillen.com"
                      style={{ all: "unset", cursor: "pointer" }}
                      target="_blank"
                      rel="noreferrer"
                    >
                      P. M. Gillen
                    </a>
                  </span>{" "}
                  | <span className="fw-normal fst-italic">Private Tutor</span>
                </h5>
                <p className="mb-1 fw-light text-uppercase">
                  October 2017 - Present
                </p>
                <p>
                  Led academic tutoring and college/graduate admissions
                  consulting sessions with individual students/families and
                  practice examination groups of 25 and more.
                </p>
                <p>
                  Improved student understanding of college-level material,
                  scores on standardized tests (SAT, ACT, AP Calculus AB/BC, AP
                  Physics B/C, LSAT, GMAT, GRE), and admissions/scholarship
                  outcomes to elite universities (Georgetown, NYU, Brown).
                </p>
              </div>
              {/* <div id="Skadden">
                <h5 className="mb-0">
                  <span className="fw-bold">
                    Skadden, Arps, Slate, Meagher & Flom, LLP
                  </span>{" "}
                  |{" "}
                  <span className="fw-normal fst-italic">Legal Assistant</span>
                </h5>
                <p className="mb-1 fw-light text-uppercase">
                  August 2014 - September 2017
                </p>
                <p>
                  Maintained accuracy and efficiency under strict deadlines when
                  drafting attorney correspondence, organizing case discovery
                  and filing databases, and preparing litigation/corporate
                  filings across a wide variety of clients.
                </p>
                <p>
                  Led team of support staff on large-scale patent litigation
                  that involved coordinating New York and Boston offices in
                  document assembly and review, resulting in favorable judgment
                  for the client.
                </p>
              </div> */}
            </div>
            <div
              className={`col-12 col-md-6 px-5 py-4${
                windowDimensions.width > 767 ? " border-start" : ""
              }`}
            >
              <div style={{ width: "auto", textAlign: "center" }}>
                <h3 className="ps-2 py-2 fw-semibold d-inline-block">
                  Application Development
                </h3>
              </div>
              <div id="Youneverse">
                <h5 className="mb-0">
                  <span className="fw-bold">Youneverse</span> |{" "}
                  <span className="fw-normal fst-italic">
                    ServiceNow Financial Banking Application
                  </span>
                </h5>
                <p className="mb-1 fw-light text-uppercase">
                  September 2023 - January 2024
                </p>
                <p>
                  Customized ServiceNow Vancouver release to meet
                  enterprise-level ITSM and customer service management (CSM)
                  needs.
                </p>
                <p>
                  Developed custom CSM case type and service portals for
                  internal and external users to submit, process, and track the
                  status of service requests.
                </p>
              </div>
              <div id="DeckReactor">
                <h5 className="mb-0">
                  <span className="fw-bold">DeckReactor</span> |{" "}
                  <span className="fw-normal fst-italic">
                    Card Collection Management Application
                  </span>
                </h5>
                <p className="mb-1 fw-light text-uppercase">November 2022</p>
                <p>
                  Magic: The Gathering card collection and deck management React
                  application that interfaces with an external API to provide
                  data about specific cards and search queries.
                </p>
                <p>
                  Configured Redux store to enable global state communication
                  between MongoDB database and React front end with FastAPI.
                </p>
              </div>
              <div id="CarCar">
                <h5 className="mb-0">
                  <span className="fw-bold">CarCar</span> |{" "}
                  <span className="fw-normal fst-italic">
                    Car Dealership Management Application
                  </span>
                </h5>
                <p className="mb-1 fw-light text-uppercase">October 2022</p>
                <p>
                  Developed auto maintenance microservice of single-page auto
                  dealership application to manage service appointments and
                  dealership employees.
                </p>
                <p>
                  Ensured successful communication among Docker containers for
                  Django back end and React front end using RESTful API and
                  microservice polling.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="Skills">
        <div className="container">
          <div className="row">
            <h1 className="col-12 text-center fw-semibold py-3">Skills</h1>
          </div>
          <div className="row">
            <div className="col-12 col-md-6 px-3 pt-4 pt-0 pt-md-4">
              <div id="Coding">
                <div className="row py-3">
                  <div className="col-12 col-md-4">
                    <h5 className="d-inline-block pe-1">Coding</h5>
                  </div>
                  <div className="col-12 col-md-8">
                    <div style={{ width: "250px" }}>
                      {[...Array(10)].map((_, i) => (
                        <span key={"coding" + i}>
                          <span
                            className={`bg-accent-4 fs-6 bi bi-circle${
                              i < 9 ? "-fill" : ""
                            }`}
                          ></span>
                          &nbsp;
                        </span>
                      ))}
                      {/* <h5 className="d-inline-block mx-2 fw-bold bg-accent-4">
                        90%
                      </h5> */}
                    </div>
                    <p className="fw-light">
                      Python 3, JavaScript, TypeScript, SQL, PHP, Bash
                    </p>
                  </div>
                </div>
              </div>
              <div id="Front End">
                <div className="row py-3">
                  <div className="col-12 col-md-4">
                    <h5 className="d-inline-block pe-1">Front End</h5>
                  </div>
                  <div className="col-12 col-md-8">
                    <div style={{ width: "250px" }}>
                      {[...Array(10)].map((_, i) => (
                        <span key={"frontend" + i}>
                          <span
                            className={`bg-accent-4 fs-6 bi bi-circle${
                              i < 9 ? "-fill" : ""
                            }`}
                          ></span>
                          &nbsp;
                        </span>
                      ))}
                      {/* <h5 className="d-inline-block mx-2 fw-bold bg-accent-4">
                        90%
                      </h5> */}
                    </div>
                    <p className="fw-light">
                      HTML5, CSS, XML, Angular, React, Bootstrap, DOM
                      Manipulation, Websockets
                    </p>
                  </div>
                </div>
              </div>
              <div id="Back End">
                <div className="row py-3">
                  <div className="col-12 col-md-4">
                    <h5 className="d-inline-block pe-1">Back End</h5>
                  </div>
                  <div className="col-12 col-md-8">
                    <div style={{ width: "250px" }}>
                      {[...Array(10)].map((_, i) => (
                        <span key={"backend" + i}>
                          <span
                            className={`bg-accent-4 fs-6 bi bi-circle${
                              i < 8 ? "-fill" : ""
                            }`}
                          ></span>
                          &nbsp;
                        </span>
                      ))}
                      {/* <h5 className="d-inline-block mx-2 fw-bold bg-accent-4">
                        80%
                      </h5> */}
                    </div>
                    <p className="fw-light">
                      Django 4, RabbitMQ, FastAPI, MySQL, PostgreSQL, MongoDB,
                      Pandas
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-6 px-3 pt-0 pt-md-4 pb-4">
              <div id="System Design">
                <div className="row py-3">
                  <div className="col-12 col-md-4">
                    <h5 className="d-inline-block pe-1">System Design</h5>
                  </div>
                  <div className="col-12 col-md-8">
                    <div style={{ width: "250px" }}>
                      {[...Array(10)].map((_, i) => (
                        <span key={"systemdesign" + i}>
                          <span
                            className={`bg-accent-4 fs-6 bi bi-circle${
                              i < 7 ? "-fill" : ""
                            }`}
                          ></span>
                          &nbsp;
                        </span>
                      ))}
                      {/* <h5 className="d-inline-block mx-2 fw-bold bg-accent-4">
                        70%
                      </h5> */}
                    </div>
                    <p className="fw-light">
                      Node.js, RESTful APIs, Docker, Microservices
                    </p>
                  </div>
                </div>
              </div>
              <div id="Tools">
                <div className="row py-3">
                  <div className="col-12 col-md-4">
                    <h5 className="d-inline-block pe-1">Tools</h5>
                  </div>
                  <div className="col-12 col-md-8">
                    <div style={{ width: "250px" }}>
                      {[...Array(10)].map((_, i) => (
                        <span key={"tools" + i}>
                          <span
                            className={`bg-accent-4 fs-6 bi bi-circle${
                              i < 8 ? "-fill" : ""
                            }`}
                          ></span>
                          &nbsp;
                        </span>
                      ))}
                      {/* <h5 className="d-inline-block mx-2 fw-bold bg-accent-4">
                        80%
                      </h5> */}
                    </div>
                    <p className="fw-light">
                      AWS, VSCode, Jira, Confluence, Git, Docker, Whimsical,
                      Insomnia
                    </p>
                  </div>
                </div>
              </div>
              <div id="ServiceNow">
                <div className="row py-3">
                  <div className="col-12 col-md-4">
                    <h5 className="d-inline-block pe-1">ServiceNow</h5>
                  </div>
                  <div className="col-12 col-md-8">
                    <div style={{ width: "250px" }}>
                      {[...Array(10)].map((_, i) => (
                        <span key={"servicenow" + i}>
                          <span
                            className={`bg-accent-4 fs-6 bi bi-circle${
                              i < 9 ? "-fill" : ""
                            }`}
                          ></span>
                          &nbsp;
                        </span>
                      ))}
                      {/* <h5 className="d-inline-block mx-2 fw-bold bg-accent-4">
                        90%
                      </h5> */}
                    </div>
                    <p className="fw-light">
                      Predictive Intelligence, Automated Test Framework (ATF),
                      Flow Designer, Application Developer User Interface
                      Creator, Integration Hub, Performance Analytics
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="Education">
        <div
          className={`container${
            windowDimensions.width <= 767 ? " text-center" : ""
          }`}
        >
          <div className="row">
            <h1 className="col-12 text-center fw-semibold py-3">Education</h1>
          </div>
          <div className="row">
            <div id="AWS SAA" className="col-12 col-md-3 px-3 py-4">
              <h5 className="mb-0 fw-bold">
                <a
                  href="https://aws.amazon.com/"
                  style={{ all: "unset", cursor: "pointer" }}
                  target="_blank"
                  rel="noreferrer"
                >
                  Amazon Web Services
                </a>
              </h5>
              <p className="mb-0 fw-normal fst-italic">
                AWS Certified Solutions Architect - Associate
              </p>
              <p className="mb-1 fw-light text-uppercase">February 2025</p>
            </div>
            <div
              id="ServiceNowCSA"
              className={`col-12 col-md-3 px-3 py-4${
                windowDimensions.width > 767 ? " border-start" : ""
              }`}
            >
              <h5 className="mb-0 fw-bold">
                <a
                  href="https://www.servicenow.com/"
                  style={{ all: "unset", cursor: "pointer" }}
                  target="_blank"
                  rel="noreferrer"
                >
                  ServiceNow
                </a>
              </h5>
              <p className="mb-0 fw-normal fst-italic">
                Certified System Administrator (CSA)
              </p>
              <p className="mb-1 fw-light text-uppercase">March 2024</p>
            </div>
            <div
              id="Columbia"
              className={`col-12 col-md-3 px-3 py-4${
                windowDimensions.width > 767 ? " border-start" : ""
              }`}
            >
              <h5 className="mb-0 fw-bold">
                <a
                  href="https://www.columbia.edu/"
                  style={{ all: "unset", cursor: "pointer" }}
                  target="_blank"
                  rel="noreferrer"
                >
                  Columbia University
                </a>
              </h5>
              <p className="mb-0 fw-normal fst-italic">
                Bachelor of Arts in Physics
              </p>
              <p className="mb-1 fw-light text-uppercase">2010 - 2014</p>
            </div>
            <div
              id="HackReactor"
              className={`col-12 col-md-3 px-3 py-4${
                windowDimensions.width > 767 ? " border-start" : ""
              }`}
            >
              <h5 className="mb-0 fw-bold">
                <a
                  href="https://www.galvanize.com/hack-reactor/"
                  style={{ all: "unset", cursor: "pointer" }}
                  target="_blank"
                  rel="noreferrer"
                >
                  Hack Reactor by Galvanize
                </a>
              </h5>
              <p className="mb-0 fw-normal fst-italic">
                Advanced Software Engineering Certificate
              </p>
              <p className="mb-1 fw-light text-uppercase">November 2022</p>
            </div>
            {/* <div
              id="Mahopac"
              className={`col-12 col-md-3 px-3 py-4${
                windowDimensions.width > 767 ? " border-start" : ""
              }`}
            >
              <h5 className="mb-0 fw-bold">
                <a
                  href="https://mhs.mahopac.org/"
                  style={{ all: "unset", cursor: "pointer" }}
                  target="_blank"
                  rel="noreferrer"
                >
                  Mahopac High School
                </a>
              </h5>
              <p className="mb-0 fw-normal fst-italic">Valedictorian</p>
              <p className="mb-1 fw-light text-uppercase">2007 - 2010</p>
            </div> */}
          </div>
        </div>
      </section>

      <section id="Portfolio">
        {/* <div className="container">
            <div className="row">
                <h1 className="col-12 text-center fw-semibold py-3">My Work</h1>
            </div>
            <div className="row">
                <div className="mx-auto col-10 row">
                    <div className="col-12 col-md-4">
                        <img className="w-100" style={{"border-radius": "12px"}} src="img/portfolio-1.jpeg" alt=""></img>
                        <h5 className="pt-3">Lorem ipsum</h5>
                        <p className="pb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                    </div>
                    <div className="col-12 col-md-4 square">
                        <img className="w-100" style={{"border-radius": "12px"}} src="img/portfolio-2.jpeg" alt=""></img>
                        <h5 className="pt-3">Lorem ipsum</h5>
                        <p className="pb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                    </div>
                    <div className="col-12 col-md-4 square">
                        <img className="w-100" style={{"border-radius": "12px"}} src="img/portfolio-3.jpeg" alt=""></img>
                        <h5 className="pt-3">Lorem ipsum</h5>
                        <p className="pb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="mx-auto col-10 row">
                    <div className="col-12 col-md-4 square">
                        <img className="w-100" style={{"border-radius": "12px"}} src="img/portfolio-4.jpeg" alt=""></img>
                        <h5 className="pt-4">Lorem ipsum</h5>
                        <p className="pb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                    </div>
                    <div className="col-12 col-md-4 square">
                        <img className="w-100" style={{"border-radius": "12px"}} src="img/portfolio-5.jpeg" alt=""></img>
                        <h5 className="pt-4">Lorem ipsum</h5>
                        <p className="pb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                    </div>
                    <div className="col-12 col-md-4 square">
                        <img className="w-100" style={{"border-radius": "12px"}} src="img/portfolio-6.jpeg" alt=""></img>
                        <h5 className="pt-4">Lorem ipsum</h5>
                        <p className="pb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                    </div>
                </div>
            </div>
        </div> */}
      </section>
    </div>
  );
};

ResumeHTML.propTypes = {};

ResumeHTML.defaultProps = {};

export default ResumeHTML;
