import React, { useEffect } from "react";
import styles from "./Resume.module.css";

const Resume = (props) => {
  useEffect(() => {
    props.newColor("--resume-color");
  });

  return (
    <div className={styles.Resume}>
      <iframe
        title="resume"
        src={process.env.PUBLIC_URL + "/Devin Wright - Resume.pdf"}
        width="100%"
        height="1200px"
      />
    </div>
  );
};

Resume.propTypes = {};

Resume.defaultProps = {};

export default Resume;
