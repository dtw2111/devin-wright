import React from "react";
// import PropTypes from "prop-types";
import "./Footer.css";

const Footer = (props) => {
  return (
    <div className="Footer">
      <div>
        <div className="text-secondary d-flex flex-wrap justify-content-center">
          <span
            className="mx-3"
            onMouseEnter={() => props.setCallText(true)}
            onMouseLeave={() => props.setCallText(false)}
          >
            <a
              href="tel:18452764609"
              className="link-secondary"
              target="_blank"
              rel="noreferrer"
            >
              <i
                className={`bi bi-telephone-fill px-2${
                  props.callText ? " show" : ""
                }`}
              ></i>
              <span className={`FadeIn${props.callText ? " show" : ""}`}>
                Phone
              </span>
            </a>
          </span>
          <span
            className="mx-3"
            onMouseEnter={() => props.setMailText(true)}
            onMouseLeave={() => props.setMailText(false)}
          >
            <a
              href="mailto:devin.wright.software@gmail.com"
              className="link-secondary"
              target="_blank"
              rel="noreferrer"
            >
              <i
                className={`bi bi-envelope-fill px-2${
                  props.mailText ? " show" : ""
                }`}
              ></i>
              <span className={`FadeIn${props.mailText ? " show" : ""}`}>
                Email
              </span>
            </a>
          </span>
          <span
            className="mx-3"
            onMouseEnter={() => props.setLinkedInText(true)}
            onMouseLeave={() => props.setLinkedInText(false)}
          >
            <a
              href="https://linkedin.com/in/devintwright"
              className="link-secondary"
              target="_blank"
              rel="noreferrer"
            >
              <i
                className={`bi bi-linkedin px-2${
                  props.linkedInText ? " show" : ""
                }`}
              ></i>
              <span className={`FadeIn${props.linkedInText ? " show" : ""}`}>
                LinkedIn
              </span>
            </a>
          </span>
          <span
            className="mx-3"
            onMouseEnter={() => props.setGitLabText(true)}
            onMouseLeave={() => props.setGitLabText(false)}
          >
            <a
              href="https://gitlab.com/dtw2111"
              className="link-secondary"
              target="_blank"
              rel="noreferrer"
            >
              <i
                className={`bi bi-gitlab px-2${
                  props.gitLabText ? " show" : ""
                }`}
              ></i>
              <span className={`FadeIn${props.gitLabText ? " show" : ""}`}>
                GitLab
              </span>
            </a>
          </span>
        </div>
      </div>
    </div>
  );
};

Footer.propTypes = {};

Footer.defaultProps = {};

export default Footer;
