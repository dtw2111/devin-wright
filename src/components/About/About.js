import React, { useEffect, useState } from "react";
// import PropTypes from "prop-types";
import styles from "./About.module.css";

const About = (props) => {
  useEffect(() => {
    props.newColor("--about-color");
  });

  // time calculations
  const d = new Date();
  const [y, m] = [d.getFullYear(), d.getMonth()];
  const age = m < 11 ? y - 1993 : y - 1992;

  // footer handlers
  const showMail = () => props.setMailText(true);
  const hideMail = () => props.setMailText(false);
  const showCall = () => props.setCallText(true);
  const hideCall = () => props.setCallText(false);

  // paragraph fade in animation
  const [pFadeIn, setPFadeIn] = useState(0);
  useEffect(() => {
    const pFadeInInterval = setInterval(() => {
      if (pFadeIn < bodyText.length) {
        setPFadeIn(pFadeIn + 1);
      }
    }, 750);
    return () => clearInterval(pFadeInInterval);
  }, [pFadeIn]);

  const bodyText = [
    <>
      My name is Devin Wright. I'm a {age}-year-old software developer from
      Mahopac, NY.
    </>,
    <>
      I have been learning software for the past {y - 2022} years. During that
      time, I've built several single-page web applications using frameworks
      like Django, Angular, and React.
    </>,
    <>
      I'm comfortable using complex logic in Python and JavaScript to configure
      data across the full stack using RESTful APIs, and I have interacted with
      various database management systems like MongoDB and MySQL.
    </>,
    <>
      Recently, I've been expanding my knowledge of digital infrastructure and
      SaaS development through becoming certified as an AWS Solutions Architect,
      as well as a Certified System Administrator in ServiceNow.
    </>,
    <>
      Since 2017, I've also worked in education as a college and test prep
      tutor.
    </>,
    <>
      If you're an employer interested in working with me, I encourage you to
      download a copy of my{" "}
      <a href={`${process.env.PUBLIC_URL}/resume`}>resume</a> for your records.
      Please also feel free to reach out to me by{" "}
      <a href="tel:18452764609" onMouseEnter={showCall} onMouseLeave={hideCall}>
        phone
      </a>{" "}
      or{" "}
      <a
        href="mailto:devin.wright.software@gmail.com"
        onMouseEnter={showMail}
        onMouseLeave={hideMail}
        target="_blank"
        rel="noopener noreferrer"
      >
        email
      </a>{" "}
      if you have any questions.
    </>,
  ];

  return (
    <div className={styles.About}>
      <div style={{ maxWidth: "500px", margin: "100px auto" }}>
        <div className="mx-4">
          <h1 className="display-1">Greetings!</h1>
          <div>
            {bodyText.map((paragraph, i) => {
              return (
                <p
                  className={`${
                    i >= pFadeIn ? styles.hidden : styles.fadeInUp
                  }`}
                  key={`paragraph${i + 1}`}
                >
                  {paragraph}
                </p>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

About.propTypes = {};

About.defaultProps = {};

export default About;
