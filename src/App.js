import "./App.css";
import "bootstrap-icons/font/bootstrap-icons.css";
import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import Home from "./components/Home/Home";
import NotFound from "./components/NotFound/NotFound";
// import Resume from "./components/Resume/Resume";
import ResumeHTML from "./components/Resume/ResumeHTML";
import About from "./components/About/About";
import ScrollToTop from "./ScrollToTop";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  // handle changing page color
  const initialBackground = {
    backgroundColor: "var(--home-color)",
    transition: "background-color 1s",
  };
  const [backgroundStyle, setBackgroundStyle] = useState(initialBackground);
  const newColor = (color) => {
    if (backgroundStyle.backgroundColor !== `var(${color})`) {
      setBackgroundStyle((prevStyle) => {
        const newStyle = { ...prevStyle, backgroundColor: `var(${color})` };
        return newStyle;
      });
    }
  };
  const backgroundProps = { backgroundStyle, newColor };

  // handle navbar toggle
  const [expanded, setExpanded] = useState(false);
  const collapseNav = () => {
    setExpanded(false);
  };
  const toggleNav = () => {
    setExpanded(!expanded);
  };
  const navProps = {
    expanded,
    setExpanded,
    collapseNav,
    toggleNav,
  };

  // handle footer text animation
  const [callText, setCallText] = useState(false);
  const [mailText, setMailText] = useState(false);
  const [linkedInText, setLinkedInText] = useState(false);
  const [gitLabText, setGitLabText] = useState(false);
  const footerProps = {
    callText,
    setCallText,
    mailText,
    setMailText,
    linkedInText,
    setLinkedInText,
    gitLabText,
    setGitLabText,
  };

  return (
    <div className="page-container" style={backgroundStyle}>
      <div className="content-wrap">
        <BrowserRouter basename={basename}>
          <ScrollToTop />
          <Navbar {...navProps} />
          <div
            className="highlight"
            style={{ paddingTop: "63px", paddingBottom: "34px" }}
            onClick={collapseNav}
          >
            <Routes>
              <Route path="/" element={<Home {...backgroundProps} />} />
              <Route
                path="/resume"
                element={<ResumeHTML {...backgroundProps} />}
              />
              <Route
                path="/about"
                element={<About {...{ ...footerProps, ...backgroundProps }} />}
              />
              <Route path="*" element={<NotFound {...backgroundProps} />} />
            </Routes>
          </div>
        </BrowserRouter>
      </div>
      <div onClick={collapseNav}>
        <Footer {...footerProps} />
      </div>
    </div>
  );
}

export default App;
